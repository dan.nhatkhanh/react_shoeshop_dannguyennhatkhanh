import logo from "./logo.svg";
import "./App.css";
import ShoeLayout from "./ShoeLayout/ShoeLayout";

function App() {
  return (
    <div className="App">
      <ShoeLayout />
    </div>
  );
}

export default App;
