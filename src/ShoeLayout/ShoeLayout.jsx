import React, { Component } from "react";
import { dataArr } from "./data_shoe";
import DetailShoe from "./DetailShoe";
import GioHang from "./GioHang";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataArr,
    detailShoe: dataArr[0],
    cart: [],
  };

  handleShowDetail = (id) => {
    let shoe = this.state.shoeArr.find((item) => item.id === id);

    this.setState({
      detailShoe: shoe,
    });
  };

  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];

    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });

    if (index == -1) {
      let numberProduct = { ...shoe, soLuong: 1 };
      cloneCart.push(numberProduct);
    } else {
      cloneCart[index].soLuong++;
    }

    this.setState({
      cart: cloneCart,
    });
  };

  handlePlusProduct = (shoe) => {
    let cloneCart = [...this.state.cart];

    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });

    cloneCart[index].soLuong++;

    this.setState({
      cart: cloneCart,
    });
  };

  handleReduceProduct = (shoe) => {
    let cloneCart = [...this.state.cart];

    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });

    if (cloneCart[index].soLuong > 1) {
      cloneCart[index].soLuong--;
    }

    this.setState({
      cart: cloneCart,
    });
  };

  handleRemoveProduct = (shoe) => {
    let cloneCart = [...this.state.cart];

    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });

    if (index !== -1) {
      cloneCart.splice(cloneCart[index], 1);
    }

    this.setState({
      cart: cloneCart,
    });
  };

  render() {
    return (
      <div>
        <GioHang
          handlePlusProduct={this.handlePlusProduct}
          cart={this.state.cart}
          handleReduceProduct={this.handleReduceProduct}
          handleRemoveProduct={this.handleRemoveProduct}
        />
        <ListShoe
          data={this.state.shoeArr}
          handleShowDetail={this.handleShowDetail}
          handleAddToCart={this.handleAddToCart}
        />
        <DetailShoe detailShoe={this.state.detailShoe} />
      </div>
    );
  }
}
