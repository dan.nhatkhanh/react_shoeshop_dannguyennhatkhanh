import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { name, price, description, image } = this.props.detailShoe;
    return (
      <div className="container p-5">
        <div className="row">
          <div className="col-4">
            <img src={image} alt="" className="w-100" />
          </div>

          <div className="col-8 text-left ">
            <p>
              <span className="font-weight-bold">Tên:</span> {name}
            </p>
            <p>
              <span className="font-weight-bold">Giá:</span> {price}
            </p>
            <p>
              <span className="font-weight-bold">Mô tả:</span> {description}
            </p>
          </div>
        </div>
      </div>
    );
  }
}
